
package mikai.jschema.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines default for floating point numbers e.g. float, double
 *
 * @author  mochieng (marembo.isaiah.ochieng@zalando.de)
 * @since   Dec 1, 2016
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
public @interface IntegerDefault {

    /**
     * The default value for {@link byte}, {@link short}, {@link int}, and {@link long} and their associated wrappers.
     */
    long value();
}
