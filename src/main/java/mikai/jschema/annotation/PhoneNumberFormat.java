
package mikai.jschema.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This, ideally is a special case of {@link Pattern} but based on the json-schema-validation, is mapped to "format"
 * attribute.
 *
 * @author  mochieng (marembo.isaiah.ochieng@zalando.de)
 * @since   Dec 1, 2016
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
public @interface PhoneNumberFormat {

    /**
     * Validates the json instance phone number field to adhere to specified regex.
     */
    String value();
}
