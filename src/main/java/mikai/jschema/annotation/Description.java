
package mikai.jschema.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Provides property descriptions.
 *
 * @author  mochieng (marembo.isaiah.ochieng@zalando.de)
 * @since   Dec 1, 2016
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
public @interface Description {

    /**
     * Description of the json property.
     */
    String value();
}
