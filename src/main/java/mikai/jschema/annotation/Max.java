
package mikai.jschema.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <pre>1. Valid for all forms of number representations.</pre>
 *
 * <pre>2. Valid for string length max-length validation.</pre>
 *
 * <pre>3. Valid for array length max validation.</pre>
 *
 * @author  mochieng (marembo.isaiah.ochieng@zalando.de)
 * @since   Dec 1, 2016
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
public @interface Max {

    /**
     * Determines the maximum value expected.
     */
    long value();

    /**
     * Whether the maximum value is exclusive during the validation of a json instance.
     */
    boolean exclusive() default false;
}
