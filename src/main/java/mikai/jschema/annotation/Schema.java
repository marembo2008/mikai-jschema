
package mikai.jschema.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.atteo.classindex.IndexAnnotated;

/**
 * Identifies a java class, for which a json schema is to be generated. The annotion can be applied at class level, or
 * field or method level. The property level is only useful when the type so annotated is not within the developers
 * control, e.g. from a third party library.
 *
 * @author  mochieng (marembo.isaiah.ochieng@zalando.de)
 * @since   Dec 1, 2016
 */
@IndexAnnotated
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.TYPE, ElementType.METHOD })
public @interface Schema {

    /**
     * The id of the schema. Default is generated from the fully qualified class name, URI encoded.
     */
    String id() default "";

    /**
     * By default, the only root schema, is the class which was called on the SchemaGenerator. Any other type so
     * encountered will be treated as a definition within the root schema.
     */
    boolean root() default false;

    /**
     * A shorter description of the schema generated.
     */
    String title() default "";

    /**
     * A longer description of the schema generated.
     */
    String description() default "";

    /**
     * The json-schema, which the schema will be generated for. Currently, this must remain draft-04 version.
     */
    String version() default "http://json-schema.org/draft-04/schema#";
}
