
package mikai.jschema.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Defines default values for a string and json-serialized object types. A json object may be validated against the
 * schema generated from the type, for which this default has been defined. Can be used to define defaults for
 * {@link BigDecimal} and {@link BigInteger} or any non-primitive {@link Number} representation.
 *
 * @author  mochieng (marembo.isaiah.ochieng@zalando.de)
 * @since   Dec 1, 2016
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
public @interface Default {

    /**
     * A Default string/json value.
     */
    String value();
}
