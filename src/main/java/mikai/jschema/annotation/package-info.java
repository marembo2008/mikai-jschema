/**
 * The annotations @{@link Nonnull}, and @{@link Nullable} from the java common annotations specification are considered to represent required state of a json schema.
 *
 * An enum type will be generated to an array with element "enum"
 */

/**
 * We use the jackson {@link JsonProperty} to serialize/deserialize schema property names.
 */
