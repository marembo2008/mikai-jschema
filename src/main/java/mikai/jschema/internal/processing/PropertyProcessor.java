
package mikai.jschema.internal.processing;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

/**
 * @author  mochieng (marembo.isaiah.ochieng@zalando.de)
 * @since   Dec 1, 2016
 */
public interface PropertyProcessor {

    /**
     * Processes a single property. Returns a map of this property attributes, including schema definitions, if any. May
     * call additional property processors, to fulfill the processing of the property.
     *
     * @param   property
     * @param   propertyProcessors
     *
     * @return  - Map of attributes for processed field property.
     */
    @Nonnull
    Map<String, Object> process(@Nonnull final Field property,
            @Nonnull final List<PropertyProcessor> propertyProcessors);

    /**
     * Processes a single property. Returns a map of this property attributes, including schema definitions, if any. May
     * call additional property processors, to fulfill the processing of the property.
     *
     * @param   property
     * @param   propertyProcessors
     *
     * @return  - Map of attributes for processed method property.
     */
    @Nonnull
    Map<String, Object> process(@Nonnull final Method property,
            @Nonnull final List<PropertyProcessor> propertyProcessors);
}
