
package mikai.jschema.api;

import java.io.Writer;

import javax.annotation.Nonnull;

/**
 * @author  mochieng (marembo.isaiah.ochieng@zalando.de)
 * @since   Dec 1, 2016
 */
public interface SchemaGenerator {

    /**
     * Generates schema documents for the schema class.
     *
     * @param  writer  - to write out the document json schema.
     * @param  schema  - the class to generate json schema for.
     */
    void generate(@Nonnull final Writer writer, @Nonnull final Class<?> schema);

    /**
     * Generates schema documents for the schema class.
     *
     * @param  writer       - to write out the document json schema.
     * @param  root         - the root schema. May or may not define attributes for a schema.
     * @param  moreSchemas  - individual schema classes, which must all be non-roots.
     */
    void generate(@Nonnull final Writer writer, @Nonnull final Class<?> root, @Nonnull final Class<?>... moreSchemas);
}
